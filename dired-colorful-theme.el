;;; dired-colorful-theme.el --- Theme for dired                 -*- lexical-binding: t; -*-

;; Copyright (C) 2023 Bryan Rinders

;; Author: Bryan Rinders
;; Keywords: gruvbox theme dired
;; Version: 0.0.1

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Give dired more color with custom font faces, inspired by the cli
;; tool 'exa'.

;;; Code:

(require 'dired)

(defgroup dired-colorful
  nil
  "Options for the dired-colorful theme."
  :group 'faces)

;; Permission faces
(defconst dired-colorful-perm-dir-face 'dired-colorful-perm-dir-face
  "Face with which to highlight the file type permission bit.")
(defface dired-colorful-perm-dir-face nil
  "Face for the permission file type bit in dired buffers."
  :group 'dired-colorful)

(defconst dired-colorful-perm-read-face 'dired-colorful-perm-read-face
  "Face with which to highlight the read permission bit.")
(defface dired-colorful-perm-read-face nil
  "Face for the permission readable bit in dired buffers."
  :group 'dired-colorful)

(defconst dired-colorful-perm-write-face 'dired-colorful-perm-write-face
  "Face with which to highlight the write permission bit.")
(defface dired-colorful-perm-write-face nil
  "Face for the permission writable bit in dired buffers."
  :group 'dired-colorful)

(defconst dired-colorful-perm-exec-face 'dired-colorful-perm-exec-face
  "Face with which to highlight the execute permission bit.")
(defface dired-colorful-perm-exec-face nil
  "Face for the permission executable bit in dired buffers."
  :group 'dired-colorful)

(defconst dired-colorful-user-face 'dired-colorful-user-face
  "Face with which to highlight the user owner of the file.")
(defface dired-colorful-user-face nil
  "Face for the file's user owner in dired buffers."
  :group 'dired-colorful)

(defconst dired-colorful-group-face 'dired-colorful-group-face
  "Face with which to highlight the group owner of the file.")
(defface dired-colorful-group-face nil
  "Face for file's group owner in dired buffers."
  :group 'dired-colorful)

(defconst dired-colorful-file-size-face 'dired-colorful-file-size-face
  "Face with which to highlight the file size.")
(defface dired-colorful-file-size-face nil
  "Face for the file's size in dired buffers."
  :group 'dired-colorful)

(defconst dired-colorful-file-time-face 'dired-colorful-file-time-face
  "Face with which to highlight the modification time of the file.")
(defface dired-colorful-file-time-face nil
  "Face for file's modification time in dired buffers."
  :group 'dired-colorful)

(defvar dired-colorful-perm-font-lock-re
  (let* ((dir-perm   "\\([dlcbsp-]\\)")
         (read-perm  "\\([r-]\\)")
         (write-perm "\\([w-]\\)")
         (exec-perm  "\\([xsgt-]\\)")
         (permissions (concat read-perm write-perm exec-perm
                              read-perm write-perm exec-perm
                              read-perm write-perm exec-perm))
         (user       "\\([^ ]+\\)")
         (group      "\\([^ ]+\\)")
         ;; extra column used by character and block devices; no idea
         ;; what it means
         (optional   "\\(?:[0-9]+, +\\)?")
         (file-size  "\\([^ ]+\\)")
         (file-time  "\\([^ ]+ +[^ ]+ +[^ ]+\\)"))
    (format "^. %s%s\\+? +[^ ]+ %s+ +%s +%s *%s %s "
            dir-perm
            permissions
            user
            group
            optional
            file-size
            file-time))
  "Regexp to identify alphanumeric permission mode strings. This
constant is used to colorize the string, using `font-lock-mode'.")

;;; Files
;; No need to take into account the extra column used by character and
;; block devices since those files have the `dired-special' face.
(defvar dired-colorful-prefix-re
  "^. %s[^ ]* +[^ ]+ +[^ ]+ +[^ ]+ +[^ ]+ +[^ ]+ +[^ ]+ +[^ ]+ \\(%s\\)$"
  "Helper regular expression used as prefix for skipping over all
  non file name characters. It contains a '%s' to allow for
  locating directories, symlinks and executables and a second
  '%s' that selects the file name itself.

  For executables use '-[^ ]*x' as replacement for the first
  '%s', without the quotes.")

(defconst dired-colorful-executable-face 'dired-colorful-executable-face
  "Face with which to highlight executable files.")
(defface dired-colorful-executable-face nil
  "Dired face for executable files"
  :group 'dired-colorful)
(defvar dired-colorful-font-lock-executable-re
  (format dired-colorful-prefix-re "-[^ ]*x" ".*")
  "Regular expression for locating executables in dired buffers.")

(defconst dired-colorful-images-face 'dired-colorful-images-face
  "Face with which to highlight images files.")
(defface dired-colorful-images-face nil
  "Dired face for image files"
  :group 'dired-colorful)
(defvar dired-colorful-font-lock-images-re
  (format dired-colorful-prefix-re "-" ".*jpg\\|.*jpeg\\|.*png")
  "Regular expression for locating images in dired buffers.")

(defconst dired-colorful-audio-face 'dired-colorful-audio-face
  "Face with which to highlight audio files.")
(defface dired-colorful-audio-face nil
  "Dired face for audio files"
  :group 'dired-colorful)
(defvar dired-colorful-font-lock-audio-re
  (format dired-colorful-prefix-re "-" ".*mp3\\|.*m4a\\|.*wav")
  "Regular expression for locating audio files in dired buffers.")

(defconst dired-colorful-video-face 'dired-colorful-video-face
  "Face with which to highlight video files.")
(defface dired-colorful-video-face nil
  "Dired face for video files"
  :group 'dired-colorful)
(defvar dired-colorful-font-lock-video-re
  (format dired-colorful-prefix-re "-" ".*mkv\\|.*mp4\\|.*webm")
  "Regular expression for locating video files in dired buffers.")

(defconst dired-colorful-archive-face 'dired-colorful-archive-face
  "Face with which to highlight archives.")
(defface dired-colorful-archive-face nil
  "Dired face for archive files"
  :group 'dired-colorful)
(defvar dired-colorful-font-lock-archive-re
  (format dired-colorful-prefix-re "-" ".*zip\\|.*gz\\|.*tar\\|.*tgz\\|.*iso\\|rpm\\|.*deb")
  "Regular expression for locating archives in dired buffers.")

(defconst dired-colorful-pdf-face 'dired-colorful-pdf-face
  "Face with which to highlight pdfs.")
(defface dired-colorful-pdf-face nil
  "Dired face for pdf files"
  :group 'dired-colorful)
(defvar dired-colorful-font-lock-pdf-re
  (format dired-colorful-prefix-re "-" ".*pdf")
  "Regular expression for locating pdf'sin dired buffers.")

(defconst dired-colorful-opendoc-face 'dired-colorful-opendoc-face
  "Face with which to highlight opendoc files.")
(defface dired-colorful-opendoc-face nil
  "Dired face for open document files"
  :group 'dired-colorful)
(defvar dired-colorful-font-lock-opendoc-re
  (format dired-colorful-prefix-re "-" ".*csv\\|.*od[st]\\|.*xls[mx]?\\|.*doc[mx]?")
  "Regular expression for locating open documents in dired buffers.")

(defconst dired-colorful-gpg-face 'dired-colorful-gpg-face
  "Face with which to highlight encrypted files.")
(defface dired-colorful-gpg-face nil
  "Dired face for encrypted files and directories"
  :group 'dired-colorful)
(defvar dired-colorful-font-lock-gpg-re
  (format dired-colorful-prefix-re "-" ".*gpg")
  "Regular expression for locating open documents in dired buffers.")

(defvar dired-colorful-face-font-lock-keywords
  (list
   ;; permissions, user, group, file size and time string
   (list dired-colorful-perm-font-lock-re
         '(1  dired-colorful-perm-dir-face)
         '(2  dired-colorful-perm-read-face)
         '(3  dired-colorful-perm-write-face)
         '(4  dired-colorful-perm-exec-face)
         '(5  dired-colorful-perm-read-face)
         '(6  dired-colorful-perm-write-face)
         '(7  dired-colorful-perm-exec-face)
         '(8  dired-colorful-perm-read-face)
         '(9  dired-colorful-perm-write-face)
         '(10 dired-colorful-perm-exec-face)
         '(11 dired-colorful-user-face)
         '(12 dired-colorful-group-face)
         '(13 dired-colorful-file-size-face)
         '(14 dired-colorful-file-time-face))
   ;; files
   (list dired-colorful-font-lock-archive-re    1 'dired-colorful-archive-face)
   (list dired-colorful-font-lock-audio-re      1 'dired-colorful-audio-face)
   (list dired-colorful-font-lock-executable-re 1 'dired-colorful-executable-face)
   (list dired-colorful-font-lock-gpg-re        1 'dired-colorful-gpg-face)
   (list dired-colorful-font-lock-images-re     1 'dired-colorful-images-face)
   (list dired-colorful-font-lock-opendoc-re    1 'dired-colorful-opendoc-face)
   (list dired-colorful-font-lock-pdf-re        1 'dired-colorful-pdf-face)
   (list dired-colorful-font-lock-video-re      1 'dired-colorful-video-face))
  "Dired-Colorful `font-lock-mode' keywords definition for file
details and file names.")

(font-lock-add-keywords 'dired-mode dired-colorful-face-font-lock-keywords t)
(font-lock-add-keywords 'wdired-mode dired-colorful-face-font-lock-keywords t)

(deftheme dired-colorful
  "Theme to make dired more colorful, based on the Gruvbox color
palette.")

(custom-theme-set-faces
 'dired-colorful
 ;; Customize file faces
 '(dired-colorful-archive-face    ((t (:foreground "#8ec07c"))))
 '(dired-colorful-audio-face      ((t (:foreground "#b16286"))))
 '(dired-colorful-executable-face ((t (:foreground "#fabd2f" :weight medium))))
 '(dired-colorful-gpg-face        ((t (:foreground "#8ec07c"))))
 '(dired-colorful-images-face     ((t (:foreground "#d79921"))))
 '(dired-colorful-opendoc-face    ((t (:foreground "#b8bb26"))))
 '(dired-colorful-pdf-face        ((t (:foreground "#d3869b"))))
 '(dired-colorful-video-face      ((t (:foreground "#d65d0e"))))
 ;; Customize permission, user, etc, faces
 '(dired-colorful-perm-dir-face   ((t (:foreground "#5fd700" :weight medium))))
 '(dired-colorful-perm-read-face  ((t (:foreground "#fabd2f"))))
 '(dired-colorful-perm-write-face ((t (:foreground "#d70000"))))
 '(dired-colorful-perm-exec-face  ((t (:foreground "#5fd700"))))
 '(dired-colorful-user-face       ((t (:foreground "#fabd2f"))))
 '(dired-colorful-group-face      ((t (:foreground "#fabd2f"))))
 '(dired-colorful-file-size-face  ((t (:foreground "#5fd700"))))
 '(dired-colorful-file-time-face  ((t (:foreground "#83a598"))))
 ;; Customize default dired faces
 '(dired-perm-write ((t (:foreground "#d70000" :inherit dired-colorful-perm-write-face :underline unspecified))))
 '(dired-marked     ((t (:foreground "#efd6a6" :background "#815b13" :underline t))))
 '(dired-flagged    ((t (:foreground "#ff99aa" :background "#5a0000" :underline t))))
 '(dired-directory  ((t (:foreground "#b8df26" :weight semibold))))
 )

;; Autoload for MELPA

;;;###autoload
(and load-file-name
     (boundp 'custom-theme-load-path)
     (add-to-list 'custom-theme-load-path
                  (file-name-as-directory
                   (file-name-directory load-file-name))))

(provide-theme 'dired-colorful)
(provide 'dired-colorful-theme)
;;; dired-colorful-theme.el ends here
